﻿using System;
using System.Diagnostics;
using TrustpilotChallenge;

namespace AnagramChallenge.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const string phrase = "poultryoutwitsants";
            const string solutionHash = "4624d200580677270a54ccff86b9610e";
            const int minWordLength = 3;
            const int maxPhraseLength = 3;
            const int threadsCount = 4; // optimal value for MacBook i7-4870HQ CPU @ 2.5GHz

            var stopwatch = Stopwatch.StartNew();

            var alphabet = new Alphabet(phrase);
            var dictionary = AnagramDictionary.LoadFromFile(@"wordlist", alphabet,  word => word.Length >= minWordLength);
            var anagramSolver = new AnagramSolver(phrase,  alphabet, dictionary, maxPhraseLength, threadsCount);
            
            anagramSolver.OnSolutionFound += (sender, solution) =>
            {
                if (solution.ToMD5() == solutionHash)
                {
                    stopwatch.Stop();
                    Console.WriteLine(solution);
                    Console.WriteLine(stopwatch.Elapsed);
                }
            };
            anagramSolver.FindSolutions();

            Console.Read();
        }
    }
}