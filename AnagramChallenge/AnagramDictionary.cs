using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;

namespace TrustpilotChallenge
{
    /// <summary>
    /// Repseresnt a dictionary optimized for anagram lookup 
    /// where words are grouped by their prime number product representation
    /// </summary>
    public class AnagramDictionary
    {
        private readonly Dictionary<BigInteger, List<string>> _words;

        public BigInteger[] Words => _words.Keys.ToArray();

        public List<string> this[BigInteger word] => _words[word];

        /// <summary>
        /// Loads wordlist from specified path and applies filters
        /// </summary>
        public static AnagramDictionary LoadFromFile(string path, Alphabet alphabet, Func<string, bool> predicate)
        {
            var words = File.ReadAllLines(path)
                .Select(word => word.ToLower())
                .OrderBy(word => word)
                .Where(alphabet.CanFit)
                .Where(predicate)
                .Distinct();
            return new AnagramDictionary(InitializeWordList(alphabet, words));
        }

        private AnagramDictionary(Dictionary<BigInteger, List<string>> words)
        {
            _words = words;
        }

        /// <summary>
        /// Groups words by their numeric representation
        /// </summary>
        private static Dictionary<BigInteger, List<string>> InitializeWordList(Alphabet abc, IEnumerable<string> words)
        {
            var dictionary = new Dictionary<BigInteger, List<string>>();
            foreach (var word in words)
            {
                var numericWord = abc.NumericWordFor(word);
                if (!dictionary.ContainsKey(numericWord))
                {
                    dictionary[numericWord] = new List<string>();
                }
                dictionary[numericWord].Add(word);
            }
            return dictionary;
        }
    }
}