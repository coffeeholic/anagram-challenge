﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace TrustpilotChallenge
{
    /// <summary>
    /// Alphabet where letter are replaced with prime numbers.
    /// </summary>
    public class Alphabet
    {
        /// <summary>
        /// Retuns a prime number assigned to a letter
        /// </summary>
        public int this[char letter] => _primeAlphabet[letter];

        /// <summary>
        /// Creates an alphabet for a specific phrase
        /// </summary>
        /// <param name="phrase"></param>
        public Alphabet(string phrase)
        {
            int[] primes = {
                2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101
            };
            
            _allLetters = phrase.OrderBy(c => c).ToArray();

            char[] alphabet = _allLetters.Distinct().ToArray();
            _primeAlphabet = new Dictionary<char, int>(alphabet.Length);
            for (int letterIndex = 0; letterIndex < alphabet.Length; letterIndex++)
            {
                _primeAlphabet[alphabet[letterIndex]] = primes[letterIndex];
            }
        }

        /// <summary>
        /// Convert a string into a product of prime numbers
        /// </summary>
        public BigInteger NumericWordFor(string input)
        {
            BigInteger primePhrase = 1;
            for (int i = 0; i < input.Length; i++)
            {
                primePhrase *= _primeAlphabet[input[i]];
            }
            return primePhrase;
        }

        /// <summary>
        /// Checks wether a string does not have more symbols than alphabet allows.
        /// </summary>
        public bool CanFit(string input)
        {
            var abcMap = new Dictionary<char, int>();

            foreach (var c in _allLetters)
            {
                if (abcMap.ContainsKey(c))
                    abcMap[c]++;
                else
                    abcMap[c] = 1;
            }

            foreach (var c in input)
            {
                if (_allLetters.Contains(c))
                {
                    int value = abcMap[c];
                    if (value == 0)
                    {
                        return false;
                    }
                    abcMap[c] = value - 1;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        private readonly Dictionary<char, int> _primeAlphabet;
        private readonly char[] _allLetters;
    }
}