using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace TrustpilotChallenge
{
    public class AnagramSolver
    {
        public event EventHandler<string> OnSolutionFound;

        private readonly BigInteger _phrase;
        private readonly AnagramDictionary _dictionary;
        private readonly int _maxPhraseLength;
        private readonly int _threadsCount;

        public AnagramSolver(string phrase, Alphabet alphabet, AnagramDictionary dictionary, int maxPhraseLength = 0, int threadsCount = 2)
        {
            _phrase = alphabet.NumericWordFor(phrase);
            _dictionary = dictionary;
            _maxPhraseLength = maxPhraseLength;
            _threadsCount = threadsCount;
        }

        /// <summary>
        /// Find all solutions recursively in parallel tasks
        /// </summary>
        public void FindSolutions()
        {
            var words = _dictionary.Words.ToArray();
            var tasks = new List<Task>();

            int batchSize = (int)Math.Floor((double) (words.Length / _threadsCount));
            for (int i = 0; i < _threadsCount; i++)
            {
                int start = i * batchSize;
                int end = Math.Min(start + batchSize, words.Length);

                tasks.Add(Task.Factory.StartNew(() =>
                {
                    for (int wi = start; wi < end; wi++)
                    {
                        FindSolutions(words, 1, _phrase, words[wi], new List<BigInteger>());
                    }
                }, TaskCreationOptions.LongRunning));
            }

            Task.WaitAll(tasks.ToArray());
        }
        
        private void FindSolutions(BigInteger[] words, BigInteger currentPhrase, BigInteger finalPhrase, BigInteger currentWord, List<BigInteger> phrase)
        {
            currentPhrase *= currentWord;
            phrase.Add(currentWord);

            // not enough letters for a phrase
            if (finalPhrase % currentPhrase != 0)
            {
                return;
            }

            // found matching phrase
            if (finalPhrase == currentPhrase)
            {
                var unwrappedWords = phrase.Select(w => _dictionary[w]).ToList();
                foreach (var solvedPhrase in GetCombinations(unwrappedWords, new List<string>()))
                {
                    OnSolutionFound?.Invoke(this, solvedPhrase.Concat(" "));
                }
            }

            // phrase exeeds amount of words
            if (_maxPhraseLength > 0 && phrase.Count >= _maxPhraseLength)
            {
                return;
            }

            // backtrack a dictionary with new phrase
            for (int wi = 0; wi < words.Length; wi++)
            {
                FindSolutions(words, currentPhrase, finalPhrase, words[wi], new List<BigInteger>(phrase));
            }
        }

        private IEnumerable<List<T>> GetCombinations<T>(IEnumerable<List<T>> lists, IEnumerable<T> selected)
        {
            if (lists.Any())
            {
                var remainingLists = lists.Skip(1);
                foreach (var item in lists.First().Where(x => !selected.Contains(x)))
                    foreach (var combo in GetCombinations(remainingLists, selected.Concat(new [] { item })))
                        yield return combo; 
            }
            else
            {
                yield return selected.ToList();
            }
        }
    }
}