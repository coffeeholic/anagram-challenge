using System.Numerics;
using NUnit.Framework;

namespace TrustpilotChallenge.Tests
{
    [TestFixture]
    public class AlphabetTests
    {
        [Test]
        public void Can_create_alphabet()
        {
            string phrase = "abc";
            var abc = new Alphabet(phrase);
            Assert.That(abc['a'], Is.EqualTo(2));
            Assert.That(abc['b'], Is.EqualTo(3));
            Assert.That(abc['c'], Is.EqualTo(5));
        }

        [Test]
        public void Can_create_alphabet_with_only_used_letters()
        {
            string phrase = "abcz";
            var abc = new Alphabet(phrase);
            Assert.That(abc['a'], Is.EqualTo(2));
            Assert.That(abc['b'], Is.EqualTo(3));
            Assert.That(abc['c'], Is.EqualTo(5));
            Assert.That(abc['z'], Is.EqualTo(7));
        }

        [Test]
        public void Can_convert_a_string_into_a_product_of_prime_numbers()
        {
            string phrase = "coffeeholic";
            var abc = new Alphabet(phrase);
            var word = abc.NumericWordFor("coffee");
            BigInteger expextedProduct = abc['c']*abc['o']*abc['f']*abc['f']*abc['e']*abc['e'];

            Assert.That(word, Is.EqualTo(expextedProduct));
        }

        [Test]
        public void Can_verify_if_a_word_can_fit_the_alphabet()
        {
            string phrase = "coffeeholic";
            var abc = new Alphabet(phrase);
           
            Assert.That(abc.CanFit("coffee"), Is.True);
            Assert.That(abc.CanFit("coffeecoffee"), Is.False);
            Assert.That(abc.CanFit("tea"), Is.False);
        }
    }
}