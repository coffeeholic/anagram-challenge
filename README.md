A solution to anagram problem found on stackoverflow.com:

*We have a message for you. But we hid it. *
*Unless you know the secret phrase, it will remain hidden.*

*Can you write the algorithm to find it?*

*Here is a couple of important hints to help you out:*
*- An anagram of the phrase is: "poultry outwits ants"*
*- The MD5 hash of the secret phrase is "4624d200580677270a54ccff86b9610e"*
*Here is a list of English words, it should help you out.*

(List of words is in the repo)